# AddItUp
This repository contains 3 projects:
## API
> Instructions for building and running the API is [here](./WebApi/).

The is an .Net Core 2 application written in C# that provides the following endpoints for fetching an exercise and submitting answers.
### Get Exercise
`[GET] /api/exercise?playerId={player-id-guid}`

When requested without a player id, the API registers a new player and returns the player id, the question and duration in which is should be answered. It's the responsibility of the client application to make sure that the player answers within the given time.
#### Sample Response
```
{
    "playerId": "160e6763-f1bc-4b71-bb27-8d97080b1bfa",
    "question": "375 + 876 = ?",
    "durationSeconds": 10
}
```
For an existing game, a player id should be provided as query string and the next question for that player is returned.
The duration for answering the question decreaes by 1 second when three questions are answered. Any wrong answer will reset the player.

### Submit Answer
`[POST] /api/exercise`

#### Sample Request
```
{
    "playerId": "160e6763-f1bc-4b71-bb27-8d97080b1bfa",
    "question": "375 + 876 = ?",
    "answer": 1251
}
```
Submitting the answer in the above format will return the result. It's important that the player id and question match the one that was received.
#### Sample Response
```
{
    "playerId": "160e6763-f1bc-4b71-bb27-8d97080b1bfa",
    "question": "375 + 876 = ?",
    "answer": 1251,
    "isCorrect": true
}
```

## AddItUp WebApp
> Instructions for building and running the WebApp is [here](./WebApiClient/additup-app/).

This is a React/Redux/Observable app written in TypeScript with dependency on the common library below. It presents the user with a question with a time given for answering it. If the user answers correctly within the given time, they are presented with another question. If the answer is incorrect or the time is up, the game is reset.
## AddItUp Common (Front End Library)

> Instructions for building the library is [here](./WebApiClient/common/).

This library contains some reusable components written in TypeScript.
