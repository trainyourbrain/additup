# AddItUp

## Build
```
dotnet build ./WebApi/WebApi.csproj
```
## Test
```
dotnet test ./WebApi.Tests
dotnet test ./Services.Tests
```
## Run
```
dotnet run --project ./WebApi/WebApi.csproj
```
Url is hardcoded to `http://localhost:56000` in `launchSettings.json`

### Initial Exercise Duration
This can be changed in code by changing the value of `Services.ExerciseService.DefaultDuration`

### Number Range
The range in which the numbers are produced can be changed in code by changing the `const` properties of `Services.ExerciseProvider`

### Gotcha
If the `[GET] /api/exercise?playerId={player-id-guid}` endpoint is called with a playerId that doesn't exist, it throws an exception. This should be handled gracefully.