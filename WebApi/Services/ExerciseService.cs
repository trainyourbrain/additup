using System;
using Services.Models;

namespace Services
{
    public class ExerciseService: IExerciseService
    {
        private readonly IExerciseProvider _exerciseProvider;
        private readonly IPlayerRepository _playerRepository;
        public const int DefaultDuration = 10;

        public ExerciseService(IExerciseProvider exerciseProvider, IPlayerRepository playerRepository)
        {
            _exerciseProvider = exerciseProvider;
            _playerRepository = playerRepository;
        }        
        public Exercise StartGame(Guid playerId)
        {
            var exercise = _exerciseProvider.GetExercise();
            var participant = new Participant
            {
                playerId = playerId,
                CurrentQuestion = exercise.Question,
                AnswerToCurrentExercise = exercise.Answer,
                CorrectAnswerCount = 0,
                CurrentDurationSeconds = DefaultDuration
            };
            _playerRepository.Set(participant);
            
            return new Exercise
            {
                playerId = participant.playerId,
                DurationSeconds = participant.CurrentDurationSeconds,
                Question = exercise.Question
            };            
        }

        public bool IsCorrect(Guid playerId, string question, long answer)
        {
            return _playerRepository.TryGetValue(playerId, out var participant)
                   && question == participant.CurrentQuestion
                   && answer == participant.AnswerToCurrentExercise;
        }

        public Exercise GetNext(Guid playerId)
        {
            if (_playerRepository.TryGetValue(playerId, out var participant))
            {
                var exercise = _exerciseProvider.GetExercise();
                participant.CurrentQuestion = exercise.Question;
                participant.AnswerToCurrentExercise = exercise.Answer;
                if (participant.CorrectAnswerCount >= 2)
                {
                    participant.CorrectAnswerCount = 0;
                    participant.CurrentDurationSeconds--;
                }
                else
                {
                    participant.CorrectAnswerCount++;
                }
                _playerRepository.Set(participant);
                
                return new Exercise
                {
                    playerId = playerId,
                    DurationSeconds = participant.CurrentDurationSeconds,
                    Question = exercise.Question
                };
            }
            throw new Exception($"Player not found: {playerId}");
        }

        public void Reset(Guid playerId)
        {
            _playerRepository.Delete(playerId);
        }
    }

}