namespace Services
{
    public interface IExerciseProvider
    {
        (string Question, long Answer) GetExercise();
    }
}