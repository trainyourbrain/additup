﻿using System;

namespace Services.Models
{
    public class Exercise
    {
        public Guid playerId { get; set; }
        public string Question { get; set; }
        public int DurationSeconds { get; set; }
    }
}
