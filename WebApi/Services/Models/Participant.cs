using System;

namespace Services.Models
{
    public class Participant
    {
        public Guid playerId { get; set; }
        public string CurrentQuestion { get; set; }
        public long AnswerToCurrentExercise { get; set; }
        public int CorrectAnswerCount { get; set; }
        public int CurrentDurationSeconds { get; set; }
    }
}