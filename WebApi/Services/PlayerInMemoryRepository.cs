using System;
using System.Collections.Generic;
using Services.Models;

namespace Services
{
    public class PlayerInMemoryRepository: IPlayerRepository
    {
        private static readonly IDictionary<Guid, Participant> Participants
            = new Dictionary<Guid, Participant>();

        public bool TryGetValue(Guid playerId, out Participant participant)
        {
            return Participants.TryGetValue(playerId, out participant);
        }

        public void Set(Participant participant)
        {
            Participants[participant.playerId] = participant;
        }

        public void Delete(Guid playerId)
        {
            if (Participants.TryGetValue(playerId, out _))
                Participants.Remove(playerId);
        }
    }
}