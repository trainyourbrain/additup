using System;
using Services.Models;

namespace Services
{
    public interface IExerciseService
    {
        Exercise StartGame(Guid playerId);
        bool IsCorrect(Guid playerId, string question, long answer);
        Exercise GetNext(Guid playerId);
        void Reset(Guid playerId);
    }
}