using System;

namespace Services
{
    public class ExerciseProvider: IExerciseProvider
    {
        private static readonly Random Random =new Random();
        private const int RangeMin = 100;
        private const int RangeMax = 999;
        public (string Question, long Answer) GetExercise()
        {
            var n1 = Random.Next(RangeMin, RangeMax);
            var n2 = Random.Next(RangeMin, RangeMax);
            return ($"{n1} + {n2} = ?", n1 + n2);
        }
    }
}