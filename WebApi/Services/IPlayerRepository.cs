using System;
using Services.Models;

namespace Services
{
    public interface IPlayerRepository
    {
        bool TryGetValue(Guid playerId, out Participant participant);
        void Set(Participant participant);
        void Delete(Guid playerId);
    }
}