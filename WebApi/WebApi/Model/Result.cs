using System;

namespace WebApi.Model
{
    public class Result
    {
        public Guid playerId { get; set; }
        public string Question { get; set; }
        public long Answer { get; set; }
        public bool IsCorrect { get; set; }
    }
}