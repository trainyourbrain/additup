using System;
using Services.Models;

namespace WebApi.Model
{
    public class Exercise
    {
        public Guid playerId { get; set; }
        public string Question { get; set; }
        public int DurationSeconds { get; set; }

        public static Exercise From(Services.Models.Exercise exercise)
        {
            if (exercise == null) return null;
            return new Exercise
            {
                playerId = exercise.playerId,
                Question = exercise.Question,
                DurationSeconds = exercise.DurationSeconds
            };
        }
    }
}