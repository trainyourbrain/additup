using System;

namespace WebApi.Model
{
    public class ExerciseResponse
    {
        public Guid playerId { get; set; }
        public string Question { get; set; }
        public long Answer { get; set; }
    }
}