﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Services;
using WebApi.Model;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    public class ExerciseController : Controller
    {
        private readonly IExerciseService _exerciseService;

        public ExerciseController(IExerciseService exerciseService)
        {
            _exerciseService = exerciseService;
        }

        // GET api/exercise
        [HttpGet]
        public Exercise GetExercise(Guid? playerId = null)
        {
            //TODO: This class/method doing too much - creating new player as well as returning an exercise. Ideally, there player management should be separate
            return Exercise.From(playerId.HasValue
                ? _exerciseService.GetNext(playerId.Value)
                : _exerciseService.StartGame(Guid.NewGuid()));
        }


        // POST api/exercise
        [HttpPost]
        public Result PostAnswer([FromBody]ExerciseResponse exerciseResponse)
        {
            var isCorrect = _exerciseService.IsCorrect(exerciseResponse.playerId, exerciseResponse.Question,
                exerciseResponse.Answer);
            if(!isCorrect)
                _exerciseService.Reset(exerciseResponse.playerId);
            return new Result
                {
                    playerId = exerciseResponse.playerId,
                    Question = exerciseResponse.Question,
                    Answer = exerciseResponse.Answer,
                    IsCorrect = isCorrect
                };
        }
    }
}
