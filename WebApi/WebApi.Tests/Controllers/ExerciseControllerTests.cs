using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Services;
using Services.Models;
using WebApi.Controllers;
using WebApi.Model;
using Exercise = Services.Models.Exercise;

namespace WebApi.Tests.Controllers
{
    [TestClass]
    public class ExerciseControllerTests
    {
        [TestMethod]
        public void Exercise_For_New_Game()
        {
            var playerId = Guid.NewGuid();
            var mockExerciseService = new Mock<IExerciseService>();
            mockExerciseService.Setup(m => m.StartGame(It.Is<Guid>(guid => guid != Guid.Empty)))
                .Returns(new Exercise {playerId = playerId});
            var sut = new ExerciseController(mockExerciseService.Object);
            
            var actual = sut.GetExercise();
            Assert.AreEqual(playerId, actual.playerId);
            mockExerciseService.Verify(m => m.StartGame(It.Is<Guid>(x => x != Guid.Empty)), Times.Exactly(1));
        }
        
        [TestMethod]
        public void Exercise_For_Existing_Game()
        {
            var playerId = Guid.NewGuid();
            var mockExerciseService = new Mock<IExerciseService>();
            mockExerciseService.Setup(m => m.GetNext(It.Is<Guid>(guid => guid != Guid.Empty)))
                .Returns(new Exercise {playerId = playerId});
            var sut = new ExerciseController(mockExerciseService.Object);
            
            var actual = sut.GetExercise(playerId);
            Assert.AreEqual(playerId, actual.playerId);
            mockExerciseService.Verify(m => m.GetNext(It.Is<Guid>(x => x == playerId)), Times.Exactly(1));
        }
        
        [TestMethod]
        public void Correct_Response()
        {
            var playerId = Guid.NewGuid();
            const string question = "question";
            const long answer = 10L;
            var mockExerciseService = new Mock<IExerciseService>();
            mockExerciseService.Setup(m => m.IsCorrect(It.IsAny<Guid>(), It.IsAny<string>(), It.IsAny<long>())).Returns(true);
            var sut = new ExerciseController(mockExerciseService.Object);

            var actual = sut.PostAnswer(new ExerciseResponse{playerId = playerId, Question = question, Answer = answer});
            mockExerciseService.Verify(m => m.IsCorrect(It.Is<Guid>(x => x == playerId), It.Is<string>(x => x == question), It.Is<long>(x => x == answer)), Times.Exactly(1));
            Assert.AreEqual(playerId, actual.playerId);
            Assert.IsTrue(actual.IsCorrect);
        }     
        
        [TestMethod]
        public void Incorrect_Response()
        {
            var playerId1 = Guid.NewGuid();
            const string question = "question";
            const long answer = 10L;
            var mockExerciseService = new Mock<IExerciseService>();
            mockExerciseService.Setup(m => m.IsCorrect(It.IsAny<Guid>(), It.IsAny<string>(), It.IsAny<long>())).Returns(false);
            var sut = new ExerciseController(mockExerciseService.Object);

            var actual = sut.PostAnswer(new ExerciseResponse{playerId = playerId1, Question = question, Answer = answer});
            mockExerciseService.Verify(m => m.IsCorrect(It.Is<Guid>(x => x == playerId1), It.Is<string>(x => x == question), It.Is<long>(x => x == answer)), Times.Exactly(1));
            mockExerciseService.Verify(m => m.GetNext(It.Is<Guid>(x => x == playerId1)), Times.Never);
            mockExerciseService.Verify(m => m.Reset(It.Is<Guid>(x => x == playerId1)), Times.Exactly(1));
            Assert.AreEqual(playerId1, actual.playerId);
            Assert.IsFalse(actual.IsCorrect);
        }
    }
}
