using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Services.Models;

namespace Services.Tests
{
    [TestClass]
    public class ParticipantRepositoryTests
    {
        [TestMethod]
        public void TryGetValue_Positive()
        {
            var playerId = Guid.NewGuid();
            const string question = "question";
            const long answer = 10;
            var sut = new PlayerInMemoryRepository();
            sut.Set(new Participant
            {
                playerId = playerId,
                CurrentQuestion = question,
                AnswerToCurrentExercise = answer
            });
            
            Assert.IsTrue(sut.TryGetValue(playerId, out var participant));
            Assert.AreEqual(playerId, participant.playerId);
            Assert.AreEqual(question, participant.CurrentQuestion);
            Assert.AreEqual(answer, participant.AnswerToCurrentExercise);
        }
        [TestMethod]
        public void TryGetValue_Negative()
        {
            var playerId = Guid.NewGuid();
            const string question = "question";
            const long answer = 10;
            var sut = new PlayerInMemoryRepository();
            sut.Set(new Participant
            {
                playerId = playerId,
                CurrentQuestion = question,
                AnswerToCurrentExercise = answer
            });
            
            Assert.IsFalse(sut.TryGetValue(Guid.Empty, out var participant));
            Assert.IsNull(participant);
        }
        
        [TestMethod]
        public void Delete()
        {
            var playerId = Guid.NewGuid();
            const string question = "question";
            const long answer = 10;
            var sut = new PlayerInMemoryRepository();
            sut.Set(new Participant
            {
                playerId = playerId,
                CurrentQuestion = question,
                AnswerToCurrentExercise = answer
            });
            
            sut.Delete(playerId);
            Assert.IsFalse(sut.TryGetValue(playerId, out var participant));
            Assert.IsNull(participant);
        }
    }
}