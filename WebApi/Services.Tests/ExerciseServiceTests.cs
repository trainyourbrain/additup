using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Services.Models;

namespace Services.Tests
{
    [TestClass]
    public class ExerciseServiceTests
    {
        [TestMethod]
        public void StartGame()
        {
            var playerId = Guid.NewGuid();
            const string question = "question";
            const long answer = ExerciseService.DefaultDuration;

            var exerciseProviderMock = new Mock<IExerciseProvider>();
            var participantRepositoryMock = new Mock<IPlayerRepository>();
            exerciseProviderMock.Setup(m => m.GetExercise()).Returns((question, answer));

            var sut = new ExerciseService(exerciseProviderMock.Object, participantRepositoryMock.Object);
            var actual = sut.StartGame(playerId);

            exerciseProviderMock.Verify(m => m.GetExercise(), Times.Exactly(1));
            participantRepositoryMock.Verify(m => m.Set(It.Is<Participant>(x =>
                x.playerId == playerId &&
                x.CurrentQuestion == question &&
                x.AnswerToCurrentExercise == answer)));

            Assert.AreEqual(playerId, actual.playerId);
            Assert.AreEqual(question, actual.Question);
        }

        [TestMethod]
        public void IsCorrect_Invalid_User()
        {
            var playerId = Guid.NewGuid();
            Participant participant;

            var participantRepositoryMock = new Mock<IPlayerRepository>();
            participantRepositoryMock.Setup(m => m.TryGetValue(It.IsAny<Guid>(), out participant)).Returns(false);

            var sut = new ExerciseService(null, participantRepositoryMock.Object);
            var actual = sut.IsCorrect(playerId, null, 0);

            participantRepositoryMock.Verify(m => m.TryGetValue(It.Is<Guid>(x =>
                x == playerId), out participant), Times.Exactly(1));

            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void IsCorrect_Invalid_Question()
        {
            var playerId = Guid.NewGuid();
            var participant = new Participant
            {
                playerId = playerId,
                CurrentQuestion = "other-question"
            };

            var participantRepositoryMock = new Mock<IPlayerRepository>();
            participantRepositoryMock.Setup(m => m.TryGetValue(It.IsAny<Guid>(), out participant)).Returns(true);

            var sut = new ExerciseService(null, participantRepositoryMock.Object);
            var actual = sut.IsCorrect(playerId, "question", 0);

            participantRepositoryMock.Verify(m => m.TryGetValue(It.Is<Guid>(x =>
                x == playerId), out participant), Times.Exactly(1));

            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void IsCorrect_InCorrect_Answer()
        {
            var playerId = Guid.NewGuid();
            var participant = new Participant
            {
                playerId = playerId,
                CurrentQuestion = "question",
                AnswerToCurrentExercise = ExerciseService.DefaultDuration
            };

            var participantRepositoryMock = new Mock<IPlayerRepository>();
            participantRepositoryMock.Setup(m => m.TryGetValue(It.IsAny<Guid>(), out participant)).Returns(true);

            var sut = new ExerciseService(null, participantRepositoryMock.Object);
            var actual = sut.IsCorrect(playerId, "question", ExerciseService.DefaultDuration);

            participantRepositoryMock.Verify(m => m.TryGetValue(It.Is<Guid>(x =>
                x == playerId), out participant), Times.Exactly(1));

            Assert.IsTrue(actual);
        }
        
        [TestMethod]
        public void GetNext_CorrectAnswerCount_1()
        {
            var playerId = Guid.NewGuid();
            const string question = "question";
            const long answer = ExerciseService.DefaultDuration;
            var participant = new Participant
            {
                playerId = playerId,
                CorrectAnswerCount = 1,
                CurrentDurationSeconds = ExerciseService.DefaultDuration
            };

            var exerciseProviderMock = new Mock<IExerciseProvider>();
            var participantRepositoryMock = new Mock<IPlayerRepository>();

            exerciseProviderMock.Setup(m => m.GetExercise()).Returns((question, answer));
            participantRepositoryMock.Setup(m => m.TryGetValue(It.IsAny<Guid>(), out participant)).Returns(true);

            var sut = new ExerciseService(exerciseProviderMock.Object, participantRepositoryMock.Object);
            var actual = sut.GetNext(playerId);

            exerciseProviderMock.Verify(m => m.GetExercise(), Times.Exactly(1));
            participantRepositoryMock.Verify(m => m.Set(It.Is<Participant>(x =>
                x.playerId == playerId &&
                x.CurrentQuestion == question &&
                x.AnswerToCurrentExercise == answer &&
                x.CorrectAnswerCount == 2 &&
                x.CurrentDurationSeconds == ExerciseService.DefaultDuration)));

            Assert.AreEqual(playerId, actual.playerId);
            Assert.AreEqual(question, actual.Question);
            Assert.AreEqual(ExerciseService.DefaultDuration, actual.DurationSeconds);
        }
        
        [TestMethod]
        public void GetNext_CorrectAnswerCount_2()
        {
            var playerId = Guid.NewGuid();
            const string question = "question";
            const long answer = ExerciseService.DefaultDuration;
            var participant = new Participant
            {
                playerId = playerId,
                CorrectAnswerCount = 2,
                CurrentDurationSeconds = ExerciseService.DefaultDuration
            };
            var expectedCurrentDurationSeconds = ExerciseService.DefaultDuration - 1;

            var exerciseProviderMock = new Mock<IExerciseProvider>();
            var participantRepositoryMock = new Mock<IPlayerRepository>();

            exerciseProviderMock.Setup(m => m.GetExercise()).Returns((question, answer));
            participantRepositoryMock.Setup(m => m.TryGetValue(It.IsAny<Guid>(), out participant)).Returns(true);

            var sut = new ExerciseService(exerciseProviderMock.Object, participantRepositoryMock.Object);
            var actual = sut.GetNext(playerId);

            exerciseProviderMock.Verify(m => m.GetExercise(), Times.Exactly(1));
            participantRepositoryMock.Verify(m => m.Set(It.Is<Participant>(x =>
                x.playerId == playerId &&
                x.CurrentQuestion == question &&
                x.AnswerToCurrentExercise == answer &&
                x.CorrectAnswerCount == 0 &&
                x.CurrentDurationSeconds == expectedCurrentDurationSeconds)));

            Assert.AreEqual(playerId, actual.playerId);
            Assert.AreEqual(question, actual.Question);
            Assert.AreEqual(expectedCurrentDurationSeconds, actual.DurationSeconds);
        }
    
        [TestMethod]
        public void Reset()
        {
            var playerId = Guid.NewGuid();

            var participantRepositoryMock = new Mock<IPlayerRepository>();

            var sut = new ExerciseService(null, participantRepositoryMock.Object);
            sut.Reset(playerId);

            participantRepositoryMock.Verify(m => m.Delete(It.Is<Guid>(x =>
                x == playerId)), Times.Exactly(1));
        }
    }
}