import * as React from 'react'
import { shallow } from 'enzyme'
import { Button } from './button'
import { expect } from 'chai'
import sinon from 'sinon';

describe('Button SFC', () => {
  it('should have the given label', () => {
    const button = shallow(<Button label="button-label" onClick={()=>{}} />)
    expect(button.text()).to.equal('button-label')
  })

  it('should call function when clicked', () => {
    const onClickSpy = sinon.spy()
    const button = shallow(<Button label="button-label" onClick={onClickSpy} />)
    button.find('button').simulate('click')
    expect(onClickSpy.calledOnce).to.be.true
  })
})
