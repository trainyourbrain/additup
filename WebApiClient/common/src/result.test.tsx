import * as React from 'react'
import { shallow } from 'enzyme'
import { Result } from './result'
import chai from 'chai'
import chaiEnzyme from 'chai-enzyme'

chai.should();
chai.use(chaiEnzyme())

describe('Result SFC', () => {

  it('should have message', () => {
    const result = shallow(<Result message="message" buttonLabel="" onNext={()=>{}} />)
    result.find('p').should.have.text('message')
  })

})
