import * as React from "react";
import { Button } from './button';

export interface ResultProps {
  message: string
  buttonLabel: string
  onNext: () => void;
}

export const Result: React.SFC<ResultProps> = (props) => {
  return (
      <div>
        <p>{props.message}</p>
        <Button label={props.buttonLabel} onClick={props.onNext}></Button>
      </div>
  )
};