import * as React from 'react'

export interface ButtonProps {
  label: string
  disabled?: boolean
  onClick: () => void
}

export const Button: React.SFC<ButtonProps> = (props) => {

  return (
    <div>
      <button className="field"
        type="button"
        disabled={props.disabled === null || props.disabled}
        onClick={props.onClick}
      >
        {props.label}
      </button>
    </div>
  )
}