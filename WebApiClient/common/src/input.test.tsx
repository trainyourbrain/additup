import * as React from 'react'
import { shallow } from 'enzyme'
import { expect } from 'chai'
import sinon from 'sinon';
import { Input } from './input'
import chai from 'chai'
import chaiEnzyme from 'chai-enzyme'
import sinonChai from 'sinon-chai'

chai.should()
chai.use(sinonChai)
chai.use(chaiEnzyme())

describe('Input SFC', () => {
  it('should call function when changed', () => {
      const onChangeSpy = sinon.spy()
      const input = shallow(<Input name='' value='input-value' onChange={onChangeSpy} />)
      input.find('input').simulate('change', {target: {value: 'new-value'}})
      onChangeSpy.should.have.been.calledWith(undefined, 'new-value')
  })

  it('should add error class when there is an error', () => {
    const onChangeSpy = sinon.spy()
    const input = shallow(<Input name='' value='input-value' onChange={onChangeSpy}/>)
    expect(input).to.not.have.className('has-error')
    input.setProps({ error: 'Incorrect format'})
    input.should.have.className('has-error')
  })
})

