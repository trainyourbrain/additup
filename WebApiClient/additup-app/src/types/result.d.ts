export interface Result{
    playerId: string
    question: string
    answer: number
    isCorrect: boolean
}