export interface Exercise{
    playerId: string,
    question: string,
    durationSeconds: number
}