export interface ExerciseResponse
{
    playerId: string
    question: string
    answer: number
}