export interface StoreState {
    playerId: string
    durationSeconds?: number
    isCorrect?: boolean
    showLoading: boolean
    showQuestion: boolean
    showResult: boolean
    show: boolean
    question: string
    answer?: number
}