import { Exercise } from '../components/exercise'
import { StoreState, ExerciseResponse } from '../types/index'
import { connect } from 'react-redux'
import { Dispatch } from 'redux'
import * as fromActions from '../actions'

export function mapStateToProps({ playerId, question, durationSeconds, isCorrect, answer, showLoading, showQuestion, showResult }: StoreState) {
    return {
      playerId,
      question,
      durationSeconds,
      isCorrect,
      answer,
      showLoading,
      showQuestion,
      showResult,
    }
  }


  export function mapDispatchToProps(dispatch: Dispatch<fromActions.Actions>) {
    return {
      loadQuestion: (playerId: string) => dispatch(fromActions.Actions.loadQuestion(playerId)),
      submitAnswer: (response: ExerciseResponse) => dispatch(fromActions.Actions.submitAnswer(response)),
    }
  }

  export default connect(mapStateToProps, mapDispatchToProps)(Exercise)