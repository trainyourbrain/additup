import { createStore, applyMiddleware, compose } from 'redux';
import {createEpicMiddleware} from 'redux-observable'
import { ajax } from 'rxjs/ajax'
import { rootEpic } from './epics';
import { StoreState } from './types';
import * as fromActions from './actions'
import { rootReducer } from './reducers';

export function configureStore(deps = {}) {
    const epicMiddleware = createEpicMiddleware({
        dependencies: {
            ajax,
            ...deps
        }
      })
      
      const composeEnhancers =
          window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
      
      const store = createStore<StoreState, fromActions.Actions, any, any>(rootReducer, 
        {
          showLoading: true,
          showQuestion: false,
      },
       composeEnhancers(applyMiddleware(epicMiddleware))
      )
      
      epicMiddleware.run(rootEpic as any)
      
      return store
}