import { ActionsUnion, createAction } from '@martin_hotell/rex-tils'
import { ExerciseResponse, Exercise, Result } from '../types';

export const LOAD_EXERCISE = 'LOAD_EXERCISE';
export const LOAD_EXERCISE_SUCCESS = 'LOAD_EXERCISE_SUCCESS';
export const LOAD_EXERCISE_FAILURE = 'LOAD_EXERCISE_FAILURE';
export const SUBMIT_ANSWER = 'SUBMIT_ANSWER';
export const SUBMIT_ANSWER_SUCCESS = 'SUBMIT_ANSWER_SUCCESS';
export const SUBMIT_ANSWER_FAILURE = 'SUBMIT_ANSWER_FAILURE';
export const REDUCE_DURATION = 'REDUCE_DURATION';

export const Actions = {
    loadQuestion: (playerId: string) => createAction(LOAD_EXERCISE, playerId),
    loadQuestionSuccess: (response: Exercise) => createAction(LOAD_EXERCISE_SUCCESS, response),
    loadQuestionFailure: () => createAction(LOAD_EXERCISE_FAILURE),
    submitAnswer: (response: ExerciseResponse) => createAction(SUBMIT_ANSWER, response),
    submitAnswerSuccess: (response: Result) => createAction(SUBMIT_ANSWER_SUCCESS, response),
    submitAnswerFailure: () => createAction(SUBMIT_ANSWER_FAILURE),
    reduceDuration: (seconds: number) => createAction(REDUCE_DURATION, seconds),
  }
  
export type Actions = ActionsUnion<typeof Actions>
  