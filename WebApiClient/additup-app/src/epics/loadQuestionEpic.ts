import { ActionsObservable, StateObservable } from 'redux-observable'

import { ofType } from '@martin_hotell/rex-tils'
import { switchMap, map, catchError } from 'rxjs/operators'
import * as fromActions from '../actions'
import { apiUrl } from '../constants';
import { of } from 'rxjs';
import { Exercise } from '../types';


const loadQuestionEpic = (
    actions$ : ActionsObservable<fromActions.Actions>,
    state$: StateObservable<fromActions.Actions>,
    dependencies: any) => {
    const { ajax } = dependencies
    return actions$.pipe(
        ofType(fromActions.LOAD_EXERCISE),
        switchMap(({ payload }) => {
            const query = payload ? `?playerId=${payload}` : ''
            return ajax.get(`${apiUrl}/exercise${query}`)
            .pipe(
                map(data => fromActions.Actions.loadQuestionSuccess(data['response'] as Exercise)),
                catchError(() => of(fromActions.Actions.loadQuestionFailure())),
            )
        })
    )
}

export default loadQuestionEpic