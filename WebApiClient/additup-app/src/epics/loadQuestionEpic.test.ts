// tslint:disable
import { ActionsObservable, StateObservable } from 'redux-observable'
import * as fromActions from '../actions'
import loadQuestionEpic from './loadQuestionEpic';
import { of, throwError, Subject } from 'rxjs';
import { toArray } from 'rxjs/operators';

test('loadQuestionEpic should load question', () => {
    const action$ = ActionsObservable.of(
        fromActions.Actions.loadQuestion('player-id')
    )
    const deps = {
        ajax: {
            get: (url: string) => of<any>({response:{question: 'question', durationSeconds: 10, playerId: 'player-id'}})
        }
    }
    
    const state$ = new StateObservable<any>(new Subject(), {});
    const output$ = loadQuestionEpic(action$, state$, deps)

    output$
        .pipe(toArray())
        .toPromise()
        .then(actions => {
            expect(actions).toHaveLength(1)
            expect(actions[0]).toHaveProperty('type', fromActions.LOAD_EXERCISE_SUCCESS)
        })
})

test('loadQuestionEpic can fail', () => {
    const action$ = ActionsObservable.of(
        fromActions.Actions.loadQuestion('player-id')
    )
    const deps = {
        ajax: {
            get: (url: string) => throwError({})
        }
    }
    const state$ = new StateObservable<any>(new Subject(), {});
    const output$ = loadQuestionEpic(action$, state$, deps)

    output$
        .pipe(toArray())
        .toPromise()
        .then(actions => {
            expect(actions).toHaveLength(1)
            expect(actions[0]).toHaveProperty('type', fromActions.LOAD_EXERCISE_FAILURE)
        })

})