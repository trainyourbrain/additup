import { ActionsObservable, StateObservable } from 'redux-observable'
import { interval } from 'rxjs'
import { ofType } from '@martin_hotell/rex-tils'
import { switchMap, map, take, takeUntil } from 'rxjs/operators'
import * as fromActions from '../actions'

const startTimerEpic = (
    actions$ : ActionsObservable<fromActions.Actions>,
    state$: StateObservable<fromActions.Actions>,
    dependencies: any) => {
    const { scheduler } = dependencies
    return actions$.pipe(
        ofType(fromActions.LOAD_EXERCISE_SUCCESS),
        switchMap(({ payload }) => {
            const durationSeconds = payload.durationSeconds;
            return interval(1000, scheduler).pipe(
                take(durationSeconds),
                takeUntil(actions$.ofType(fromActions.SUBMIT_ANSWER)),
                map(t => fromActions.Actions.reduceDuration(1))
            )
        })
    )
}

export default startTimerEpic