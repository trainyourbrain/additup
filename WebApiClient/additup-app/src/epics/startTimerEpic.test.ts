import { ActionsObservable, StateObservable } from 'redux-observable'
import * as fromActions from '../actions'
import { Subject, VirtualTimeScheduler } from 'rxjs';
import { toArray } from 'rxjs/operators';
import startTimerEpic from './startTimerEpic';

test('startTimerEpic runs countdown', () => {
    const scheduler = new VirtualTimeScheduler()
    const action$ = ActionsObservable.of(
        fromActions.Actions.loadQuestionSuccess({playerId: 'player-id', question: 'question', durationSeconds: 10}),
        scheduler)
    const deps = {
        scheduler
    }
    const state$ = new StateObservable<any>(new Subject(), {});
    const output$ = startTimerEpic(action$, state$, deps)

    output$
        .pipe(toArray())
        .toPromise()
        .then(actions => {
            expect(actions).toHaveLength(10)
            actions.forEach(action =>
                expect(action).toHaveProperty('type', fromActions.REDUCE_DURATION))
        })

    scheduler.flush()
})
