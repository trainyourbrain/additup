import { ActionsObservable, StateObservable } from 'redux-observable'
import { ofType } from '@martin_hotell/rex-tils'
import { switchMap, map, catchError } from 'rxjs/operators'
import * as fromActions from '../actions'
import { apiUrl } from '../constants';
import { Result } from '../types';
import { of } from 'rxjs';

const headers = {
    'Content-Type': 'application/json',
}

const submitAnswerEpic = (
    actions$ : ActionsObservable<fromActions.Actions>,
    state$: StateObservable<fromActions.Actions>,
    dependencies: any) => {
    const { ajax } = dependencies
    return actions$.pipe(
        ofType(fromActions.SUBMIT_ANSWER),
        switchMap(({ payload }) => {
            return ajax.post(`${apiUrl}/exercise`, JSON.stringify(payload), headers).pipe(
                map(data => fromActions.Actions.submitAnswerSuccess(data['response'] as Result)),
                catchError(() => of(fromActions.Actions.submitAnswerFailure()))
            )
        })
    )
}

export default submitAnswerEpic