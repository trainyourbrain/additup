import loadQuestionEpic from './loadQuestionEpic'
import submitAnswerEpic from './submitAnswerEpic'
import startTimerEpic from './startTimerEpic'
import {combineEpics} from 'redux-observable'

export const rootEpic = combineEpics(loadQuestionEpic, submitAnswerEpic, startTimerEpic)