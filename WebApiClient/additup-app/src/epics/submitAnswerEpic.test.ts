import { ActionsObservable, StateObservable } from 'redux-observable'
import * as fromActions from '../actions'
import submitAnswerEpic from './submitAnswerEpic';
import { of, throwError, Subject } from 'rxjs';
import { toArray } from 'rxjs/operators';

test('submitAnswerEpic should submit question', () => {
    const action$ = ActionsObservable.of(
        fromActions.Actions.submitAnswer({playerId: 'player-id', question: 'question', answer: 10})
    )
    const deps = {
        ajax: {
            post: (url: string, body: any, headers: any) => of<any>({response:{question: 'question', durationSeconds: 10, playerId: 'player-id'}})
        }
    }
    
    const state$ = new StateObservable<any>(new Subject(), {});
    const output$ = submitAnswerEpic(action$, state$, deps)

    output$
        .pipe(toArray())
        .toPromise()
        .then(actions => {
            expect(actions).toHaveLength(1)
            expect(actions[0]).toHaveProperty('type', fromActions.SUBMIT_ANSWER_SUCCESS)
        })
})

test('submitAnswerEpic can fail', () => {
    const action$ = ActionsObservable.of(
        fromActions.Actions.submitAnswer({playerId: 'player-id', question: 'question', answer: 10})
    )
    const deps = {
        ajax: {
            post: (url: string, body: any, headers: any) => throwError({})
        }
    }
    const state$ = new StateObservable<any>(new Subject(), {});
    const output$ = submitAnswerEpic(action$, state$, deps)

    output$
        .pipe(toArray())
        .toPromise()
        .then(actions => {
            expect(actions).toHaveLength(1)
            expect(actions[0]).toHaveProperty('type', fromActions.SUBMIT_ANSWER_FAILURE)
        })

})