import * as fromActions from '../actions'
import { StoreState } from '../types/index'

export function rootReducer(state: StoreState, action: fromActions.Actions): StoreState {
  switch (action.type) { 
    case fromActions.LOAD_EXERCISE:
    return { ...state, answer: undefined, isCorrect: undefined, showQuestion: false, showResult: false, showLoading: true }
    case fromActions.SUBMIT_ANSWER:
      return { ...state, isCorrect: undefined, showQuestion: false, showResult: false, showLoading: true }
    case fromActions.LOAD_EXERCISE_SUCCESS:
      return { ...state, ...action.payload, showQuestion: true, showResult: false, showLoading: false }
    case fromActions.SUBMIT_ANSWER_SUCCESS:
      return { ...state, ...action.payload, showQuestion: true, showResult: true, showLoading: false }
    case fromActions.LOAD_EXERCISE_FAILURE:
    case fromActions.SUBMIT_ANSWER_FAILURE:
      return { ...state, showQuestion: false, showResult: false, showLoading: false }
    case fromActions.REDUCE_DURATION:
      let durationSeconds = state.durationSeconds
      if(durationSeconds) durationSeconds--
      return { ...state, durationSeconds }
  }
  return state
}