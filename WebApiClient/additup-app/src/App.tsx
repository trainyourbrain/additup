import * as React from 'react';
import './App.css';
import Game from './containers/Game';
import { StoreState } from './types';

class App extends React.Component<{}, StoreState> {
  public render() {
    return (
      <div className="App">
        <Game/>
      </div>
    );
  }
}

export default App;
