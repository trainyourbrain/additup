import * as React from 'react'
import { Button, Input, Result } from 'additup-common'
import { ExerciseResponse } from 'src/types';
 
export interface ExerciseProps {
  playerId: string,
  question: string
  durationSeconds: number,
  isCorrect?: boolean,
  showLoading: boolean
  showQuestion: boolean
  showResult: boolean
  loadQuestion: (playerId?: string) => void
  submitAnswer: (response: ExerciseResponse) => void
}

export interface ExerciseState {
  answer: number
  value: string
  error: string
}

export class Exercise extends React.Component<ExerciseProps, ExerciseState> {
  constructor(props: ExerciseProps){

    super(props)
    this.state = {
      answer: NaN,
      value: '',
      error: ''
    }
    this.onChange = this.onChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
    this.showNext = this.showNext.bind(this)
  }

  onChange(fieldName: string, value: string){
    const answer = Number(value)
    const error = Number.isNaN(answer) ? 'Not a number' : ''
    this.setState({ answer, value, error })
  }
  onSubmit(){
    this.props.submitAnswer({
      playerId: this.props.playerId,
      question: this.props.question,
      answer: this.state.answer
    })
  }
  showNext(){
    this.setState({ ...this.state, value: '', error: ''})
    if(this.props.isCorrect !== undefined &&
      this.props.isCorrect)
      this.props.loadQuestion(this.props.playerId)
    else
    this.props.loadQuestion()
  }
  componentDidMount(){
    this.props.loadQuestion()
  }
  render(){
    const isValid = this.state.value !== ''
      && this.state.error == ''
    return (
      <div className="exercise">
        {
          this.props.showLoading &&
            <div className="loading">
              {
                !Number.isNaN(this.state.answer) &&
                <p>Your answer: {this.state.answer}</p>
              } <p>Please wait...</p>
            </div>
        }
        {
          this.props.showQuestion && 
            <div className="question">
              <h2>{this.props.question}</h2>
              <h4>Time left: {this.props.durationSeconds}s</h4>
              {
                !this.props.showResult && this.props.durationSeconds > 0 &&
                  <form>
                    <Input
                      name= "Answer"
                      placeholder="Answer"
                      onChange={this.onChange}
                      {...this.state} />
                    <Button label="Submit" onClick={this.onSubmit} disabled={!isValid}/>
                  </form>  
              }
            </div>
        }
        {
          this.props.isCorrect !== undefined &&
            <div className="result">
              <Result message={"Your answer was " + (this.props.isCorrect ? "correct!" : "incorrect :(")}
                buttonLabel={this.props.isCorrect ? "Next..." : "Reset..."} onNext={this.showNext}></Result>
            </div>
        }
        {
          this.props.durationSeconds <= 0 &&
            <div className="result">
              <Result message="Time up :("
                buttonLabel="Reset..." onNext={this.showNext}></Result>
            </div>
        }
      </div>
    )
  }
}

