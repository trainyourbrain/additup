import * as React from 'react'
import { shallow } from 'enzyme'
import { Exercise } from './exercise'

import { ExerciseResponse } from 'src/types';

describe('Exercise Component', () => {
  it('should render question, result and loading', () => {
    const props = {
        playerId: '',
        question: '',
        durationSeconds: 10,
        isCorrect: undefined,
        showLoading: false,
        showQuestion: false,
        showResult: false,
        loadQuestion: (playerId?: string) => {},
        submitAnswer: (response: ExerciseResponse) => {}
      }
    const exercise = shallow(<Exercise { ...props } />)
    expect(exercise.find('.exercise').children('.loading')).toHaveLength(0);
    expect(exercise.find('.exercise').children('.question')).toHaveLength(0);
    expect(exercise.find('.exercise').children('.result')).toHaveLength(0);
    exercise.setProps({showLoading: true, showQuestion: false, showResult: false})
    expect(exercise.find('.exercise').children('.loading')).toHaveLength(1);
    expect(exercise.find('.exercise').children('.question')).toHaveLength(0);
    expect(exercise.find('.exercise').children('.result')).toHaveLength(0);
    exercise.setProps({showLoading: false, showQuestion: true, showResult: false})
    expect(exercise.find('.exercise').children('.loading')).toHaveLength(0);
    expect(exercise.find('.exercise').children('.question')).toHaveLength(1);
    expect(exercise.find('.exercise').children('.result')).toHaveLength(0);
    exercise.setProps({showLoading: false, showQuestion: false, showResult: false, durationSeconds: 0})
    expect(exercise.find('.exercise').children('.loading')).toHaveLength(0);
    expect(exercise.find('.exercise').children('.question')).toHaveLength(0);
    expect(exercise.find('.exercise').children('.result')).toHaveLength(1);
    exercise.setProps({showLoading: false, showQuestion: false, showResult: true, durationSeconds: 1, isCorrect: true})
    expect(exercise.find('.exercise').children('.loading')).toHaveLength(0);
    expect(exercise.find('.exercise').children('.question')).toHaveLength(0);
    expect(exercise.find('.exercise').children('.result')).toHaveLength(1);
    exercise.setProps({showLoading: false, showQuestion: false, showResult: true, durationSeconds: 1, isCorrect: false})
    expect(exercise.find('.exercise').children('.loading')).toHaveLength(0);
    expect(exercise.find('.exercise').children('.question')).toHaveLength(0);
    expect(exercise.find('.exercise').children('.result')).toHaveLength(1);
  })
})